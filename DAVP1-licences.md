# Droits d’auteur, respect de la vie privée, appliquer une licence à un code

Philippe Pary, 2016© CC-BY-SA
Pour PopSchool Valenciennes. http://pop.eu.com

## Choix de la licence, concrètement

### Rappel

* Licences propriétaire: sur mesures, prévues pour créer une rareté artificielle. L’objectif est commercial.
* Licences non-copyleft: travaux partagés dont peuvent dériver des versions propriétaires (WTFPL, BSD, MIT …)
* Licences copyleft: travaux partagés dont le partage dans les versions futures est garanti (GPL, MPL …)

### Document de choix des licences

Pour rester simple, nous allons partir sur 4 types de licences :

1. Propriétaire, « tous droits réservés »
2. CC-BY-SA pour ce qui n’est pas du code
3. Domaine public / WTFPL pour les petits travaux
4. GNU GPL pour le reste

**Attention, cette grille de choix est totalement subjective**

La licence BSD peut être vue comme la licence d’un monde de paix où les tenants du logiciel propriétaire comprennent et respctent le logiciel libre et y contribuent aussi souvent que possible.  
La licence GPL peut être vue comme la licence d’un monde de méfiance où les tenants du logiciel propriétaire cherchent activement à combattre le logiciel libre.

## Comment appliquer une licence ?

### Fichier LICENCE.TXT

Mettez un fichier LICENCE.TXT à la racine du projet. À l’intérieur de ce fichier, mettez un lien et le texte de la licence choisie:

* Domaine public, CC-0: https://creativecommons.org/publicdomain/zero/1.0/
* CC-BY-SA: https://creativecommons.org/licenses/by-sa/4.0/
* WTFPL: http://www.wtfpl.net/about/
* BSD: https://www.freebsd.org/copyright/freebsd-license.html
* GPL: https://www.gnu.org/licenses/gpl.html

Exemple: [fichier licence de Bootstrap](https://github.com/twbs/bootstrap/blob/master/LICENSE)

**Touchez pas à le texte: la date et l’auteur précisent le copyright sur le texte de la licence**

### Fichier LICENCES.TXT

Si vos fichiers ont des licences différentes, créez un fichier LICENCES.TXT contenant les licences de chaque section du projet

Exemple:
    Default licence is GPL, see GPL.txt file
    Bootstrap is under MIT Licence, see js/bootstrap/LICENCE file
    images and videos are under CC-BY-SA licence, see CC-BY-SA.txt, except for the one included in ext folder who each have their own copyright notice aside

Exemple: [fichier licence de wordpress](https://github.com/WordPress/WordPress/blob/master/license.txt)

### En-tête des fichiers

Il est possible d’installer une notice de copyright dans l’en-tête des fichiers de code de la manière suivante:

    /* <nom de mon projet>, 
        <mon nom> <mon mail> <année>©
        <éventuelle description du fichier>
        <licence retenue pour le fichier> */

Exemples: 
    /* Application Veille POP School
        Philippe Pary pp@pop.eu.com 2016
        Gestion des sessions
        This file is distribuated under GPL v3+ licence
        See LICENCE.txt file or https://www.gnu.org/licenses/gpl.html
    */


* (Symfony précise les auteurs dans chaque fichier)[https://github.com/symfony/symfony/blob/master/src/Symfony/Component/Config/ConfigCache.php]
* (Pasteque précise la licence dans chaque fichier)[https://github.com/ScilCoop/pasteque-server/blob/master/index.php]
