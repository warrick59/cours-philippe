# Introduction à JavaScript

Philippe Pary, 2016 dans le cadre de POPSchool Valenciennes. http://pop.eu.com  
CC-BY-SA

## Exercice 1

À l’aide d’un bouton, changez la couleur de fond de la page à l’aide d’un attribut [onclick=](https://developer.mozilla.org/docs/Web/API/GlobalEventHandlers/onclick) qui vient modifier la valeur *document.body.style.backgroundColor*

## Exercice 2

Affichez un formulaire de saisie JavaScript ([prompt()](https://developer.mozilla.org/docs/Web/API/Window/prompt)) demandant un nom et affichez le résultat via une boîte d’alerte ([alert()](https://developer.mozilla.org/docs/Web/API/Window/alert)) sous forme « *Hi, my name is … * »

## Exercice 3

Amélioration de l’exercice précédent :  
Créez un champ *&lt;input&gt;*, exploitez le résultat via un bouton, *onclick*, ou via l’attribut *[onchange](https://developer.mozilla.org/en-US/docs/Web/API/GlobalEventHandlers/onchange)* du champ de formulaire. Affichez le résultat dans une *&lt;div&gt;* à part que vous sélectionnerez via son attribut *id* et la fonction *[document.getElementById()](https://developer.mozilla.org/en-US/docs/Web/API/Document/getElementById)* et l’attribut *[innerHTML](https://developer.mozilla.org/en-US/docs/Web/API/Element/innerHTML)* de l’[élément](https://developer.mozilla.org/en-US/docs/Web/API/Element) ainsi sélectionné.

## Un peu de théorie …

### Contexte

Première spécification JS en 1995 (rappel: HTML et WWW en 1991, CSS en 1994-95)  
Comme CSS, lié au navigateur majoritaire de l’époque, NetScape, devenu Firefox.

### Principe

JavaScript est un langage de script (comme PHP) : il n’est pas compilé. Les fichiers Javascript sont en texte plat et sont interprêtés par le moteur JavaScript du navigateur web (SpiderMonkey pour Firefox, V8 pour Chrome etc.)  
Comme pour le code CSS, le code JavaScript peut être embarqué dans la page HTML via les balises &lt;script&gt;&lt;/script&gt; ou via des fichiers spécifiques via la balise [&lt;script&gt;](https://developer.mozilla.org/fr/docs/Web/HTML/Element/script). Par convention, les fichiers ont pour extension *.js*

**Tout est exécuté sur la machine de l’utilisateur**

Conséquence, vous ne pouvez pas avoir confiance dans un résultat calculé en JavaScript car l’utilisateur peut voir les variables, modifier le code etc.  
Par exemple, si vous développez un jeu de pendu en pur JavaScript, l’utilisateur peut connaître le mot tiré au sort ou encore truquer son compteur de nombre d’essais via la console de débogage.

Si des éléments nécessitent d’être certains, vous devez les faire côté serveur. En PHP par exemple.

En JavaScript vous pouvez faire les actions sans gravité : effets visuels, amélioration des formulaires, mise à jour automatique de la page, appels à des API etc.  
JavaScript s’appuie massivement sur [DOM](https://developer.mozilla.org/docs/Web/API/Document_Object_Model) pour s’intégrer dans une page web.

### DOM ?

**Document object model**, la structure d’une page web. C’est une interface de programmation qui propose une structure des documents et une méthode pour accéder aux différentes valeurs.  

On peut accéder aux élements de la page web par 3 billets : par leur type (*&lt;div&gt;*, *&lt;a&gt;* …), par classe (valeur de l’attribut *class=*) ou par identifiant (valeur dde l’atttribut *id=*)

En clair vous avez des valeurs (document.style pour accéder au style de la page) et des fonctions (document.getElementByClass pour trouver tous les éléments ayant une classe donnée)

#### Sélecteur par id
Celui qu’on utilise tout le temps …

https://developer.mozilla.org/fr/docs/Web/API/Document/getElementById

Renvoie un seul élément

#### Sélecteur par type d’élément
https://developer.mozilla.org/fr/docs/Web/API/document/getElementsByTagName

Renvoie un tableau d’éléments

#### Sélecteur par classe
https://developer.mozilla.org/fr/docs/Web/API/Document/getElementsByClassName

Renvoie un tableau d’éléments

### Et concrètement ?

Les variables ne prennent pas de *$*, contrairement au PHP : à vous de les reconnaître. Par contre les lignes se terminent bien par *;*

Tout est objet, ça signifie qu’on peut manipuler une variable comme une fonction et une fonction comme une variable. Ça signifie que les variables ont des propriétes et des fonctions à elles.

Le code peut être lancé de deux manières : soit dès le chargement de la page, soit au travers d’un appel à une fonction.  
Tout code qui est en dehors d’une fonction est exécuté quand il est lu. Tout code situé dans une fonction (**déclaration**) est exécuté uniquement quand elle est **appelée**

#### variables

    var maVariable;
    let maVariable;

La différence entre *var* et *let* est une question de portée. *var* est accessible dans toute la fonction où elle est décrite (globale si en dehors d’une fonction). *let* est accessible uniquement dans l’accolade où elle est située (globale si en dehors d’une accolade)

Il faut favoriser l’usage de *let*

#### if / else

Rien à dire, c’est comme en PHP

    if(maVariable == 1) {
        alert("La variable vaut 1");
    }
    else {
        alert("La variable ne vaut pas 1");
    }

#### for, foreach

Le for est très proche

    for (let i=0; i < 10; i++) { }

Il existe un foreach très pratique qui parcourt un tableau (notez l’admirable syntaxe)

    monTableau = [1, 3, 5, 7];
    monTableau.forEach(alert);

#### fonctions

Comme en PHP

    function maFonction(parametre1, parametre2) {
    }

Comme en JavaScript (à privilégier)

    let maFonction = function(parametre1, parametre2) {
    }

## Exercice à finir pour le prochain cours de JS

Réalisez un jeu de pendu en JavaScript

Quelques pistes :
* element.value
* maVariable.indexOf(lettreEntree) != -1
* let i = Math.random() * (max - min) + min
* let mesMots = \["salade","tomate","oignon"\]
* let motADeviner = mesMots\[i\] (c’est cadeau là …)
