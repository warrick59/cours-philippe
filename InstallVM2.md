# Installation de VirtualBox, partie 2

# Objectif

![Un chaton ?](https://upload.wikimedia.org/wikipedia/commons/thumb/f/fe/Ailurus_fulgens_RoterPanda_LesserPanda.jpg/393px-Ailurus_fulgens_RoterPanda_LesserPanda.jpg)

Après avoir mis en place VirtualBox, créé une VM Debian et installé les paquets essentiels, nous allons finir la configuration et commencer à utiliser notre serveur de développement

# PHPMyAdmin

![Hum… objectivement, non …](https://upload.wikimedia.org/wikipedia/commons/thumb/2/20/Flat_panda.jpg/450px-Flat_panda.jpg)

## Activer PHPMyAdmin

Parce que bon, en testing c’est pété …

    # ln -s /etc/phpmyadmin/apache.conf /etc/apache2/conf.available/phpmyadmin.conf
    # a2enconf phpmyadmin
    # service apache2 reload

* Commande 1: créer un lien de la conf apache proposée par PHPMyAdmin dans la configuration d’Apache2.
* Commande 2: activer la configuration
* Commande 3: recharger la nouvelle configuration dans apache

On va sur http://<ipmonserveur>/phpmyadmin et là, miracle, tout se lance.

### Et on ouvre la bête

Inspectons ce mystérieux fichier de configuration

    $ less /etc/phpmyadmin/apache.conf

Notons les points suivants

&lt;TODO : liste des choses remarquables. À compléter selon les réactions des éléves&gt;

## Entrer dans PHPMyAdmin

Sur la page, on entre « root » au login et son mot de passe MySQL au mot de passe. Incroyable, on entre.
Truc de ouf. On creuse tout ça plus tard.

# Apache et fichiers HTML

![WTF is it ?](https://upload.wikimedia.org/wikipedia/commons/thumb/2/25/Lesser_panda_standing.jpg/640px-Lesser_panda_standing.jpg)

## Préparer le chargement du code

Nous allons charger nos premières pages webs.
Par défaut, Apache HTTPd utilise /var/www/html/ comme dossier racine du serveur web

&gt; Le dossier racine du serveur web est l’endroit où le / de *http://<ipmonserveur>/* amène.

Nous allons copier les fichier du projet Wikipédia vers ce dossier.
Mais avant, on va préparer le dossier

    # chown -R www-data:www-data /var/www/
    # chmod -R 775 /var/www/

Ça se passe de commentaires. Allez relire le cours Bash ou les manpages au besoin.

### Et on ouvre la bête

Nous allons regarder un peu la configuration d’apache. Elle tient, dans notre cas, sur deux fichiers :

* /etc/apache2/apache2.conf
* /etc/apache2/sites-available/default.conf

*NB: les modules (mods) d’apache, la configuration (conf) et les sites webs (sites) sont rangés dans des dossiers &lt;type&gt;-available et s’activent avec les commandes a2enmod, a2enconf, a2ensite. Un redémarrage d’Apache est nécessaire pour les modules (# service apache2 restart) et un rechargement pour les configurations et sites webs (# service apache2 reload)*

## Charger les fichiers

On va charger le dossier du projet wikipedia.
Sur votre ordinateur :

    $ scp -r <cheminversledossier> [<identifiant>@<ipmonserveur>:/var/www/html/

    * scp est un cp via SSH. -r c’est récursif : on copie tout le dossier

## Vérifier que ça marche

Avec votre navigateur, aller sur *http://&lt;ipduserver&gt;/&lt;nomdudossier&gt;/*

Ça affiche votre projet. Bravo ! Votre première publication … en local. Vous verrez, ça sera plus ou moins pareil pour mettre en ligne.

## Qu’est-ce qu’on se fait chier

Par contre, lancer scp à chaque fois qu’on modifie un truc, surtout quand on est un gros n00b et qu’on code à tatons, c’est juste chiant.

On va donc utiliser Nautilus pour *monter* le dossier distant comme un dossier local. Trop fun.


![Documentation sur ubuntu-fr.org](https://doc.ubuntu-fr.org/ssh#monter_un_repertoire_distant_navigation_via_sftp_secure_file_transfer_protocol)

# Développer en remote

![is it a bird ? is it a plane ?](https://upload.wikimedia.org/wikipedia/commons/b/b5/Pandas_vermelho.jpg)

## Quelques conseils

* Plutôt que de faire un copier, on fera un git clone !
* Et on n’oubliera pas de commit
* Et encore moins de pusher
* Comme ça on partage ses fichiers entre sa VM et sa machine physique !

## Quelques constats

Le montage du dossier permet d’utiliser ses outils de n00b (atom, interface graphique). Ceci étant, sur le serveur, si on maîtrise un peu les outils d’adultes (vim, emacs), c’est comme si on travaillait à la maison.

À part que quand on est coupés du web, on peut plus.

D’où l’intérêt d’un git, sur lequel on commite, on pushe et on pulle.

CQFD

# See you space cowboy ...

![No, it’s a firefox !](https://upload.wikimedia.org/wikipedia/commons/thumb/d/da/Red_Panda_Face.jpg/640px-Red_Panda_Face.jpg)

![The real folk blues](http://media-cache-ak0.pinimg.com/736x/02/12/bf/0212bf5acef9edebb8551422d101f7b2.jpg)
